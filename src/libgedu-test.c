#include <gnome.h>
#include "libgedu-tfquestion.h"
#include "libgedu-question.h"
#include "libgedu-lesson.h"
#include "libgedu-score.h"

int main(int argc, char *argv[])
{
	GtkObject *object;
	GtkObject *object2;
	GtkObject *object3;
	char *suck, *suck2;
       
	gnome_init("libgedu-test", "0.1", argc, argv);
	object = libgedu_tfquestion_new_with_question("Is my name Fred?", FALSE);
       
	suck = libgedu_tfquestion_get_question(LIBGEDU_TFQUESTION(object));

	printf(suck);
	g_free(suck);

	if ( libgedu_tfquestion_get_answer(LIBGEDU_TFQUESTION(object)) == TRUE ) 
	{
		printf("%s", "Yes, it is fred\n");
	}
	else
	{
		printf("%s", "No, its not!!\n");
	}
	printf("%s", "\n ----------------- \n");
	printf("%s", "Now we check new func.\n");
	
	if (libgedu_tfquestion_check_answer(LIBGEDU_TFQUESTION(object), TRUE) != TRUE )
	{
		printf("%s", "ANSWER IS NOT CORRECT\n");
	}
	else
	{
		printf("%s", "ANSWER IS CORRECT \n");
	}

	object2 = libgedu_question_new_with_question("What is my name?", "Will");
	
	printf(libgedu_question_get_question(LIBGEDU_QUESTION(object2)));
	printf("%s", "\n");
	printf("%s", "checking if answer is will\n");
		if (libgedu_question_check_answer(LIBGEDU_QUESTION(object2), "will") != TRUE )
	{
		printf("%s", "ANSWER IS NOT CORRECT\n");
	}
	else
	{
		printf("%s", "ANSWER IS CORRECT \n");
	}
		
	object3 = libgedu_lesson_new_with_score();
	printf("%d\n", libgedu_lesson_get_length(LIBGEDU_LESSON(object3)));
	libgedu_lesson_append_question(LIBGEDU_LESSON(object3), LIBGEDU_QUESTION(object2));
	printf("%d\n", libgedu_lesson_get_length(LIBGEDU_LESSON(object3)));
	
	libgedu_lesson_increment_right(LIBGEDU_LESSON(object3));
	libgedu_lesson_increment_total(LIBGEDU_LESSON(object3));
	libgedu_lesson_increment_total(LIBGEDU_LESSON(object3));
	
	printf("%s", "The number right: ");
	printf("%d\n", libgedu_lesson_get_right(LIBGEDU_LESSON(object3)));
	printf("%s", "The total number: ");
	printf("%d\n", libgedu_lesson_get_total(LIBGEDU_LESSON(object3)));
	printf("%s", "The percent right: ");
	printf("%d %s\n", libgedu_lesson_get_right_percent(LIBGEDU_LESSON(object3)), "%");

	return 0;
}
